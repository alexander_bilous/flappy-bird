package flappyBird;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

public class FlappyBird implements ActionListener
{

    public static FlappyBird flappybird;
    public final int WIDTH = 800, HEIGHT = 800;
    public Renderer renderer;
    public Rectangle bird;
    public ArrayList<Rectangle> columns;
    public Random rand;
    public int x, yMotion;

    public FlappyBird()
    {
      JFrame jframe = new JFrame();
      Timer timer = new Timer(20, this);
      rand = new Random();

      renderer = new Renderer();
      jframe.add(renderer);
      jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      jframe.setSize(WIDTH,HEIGHT);
      jframe.setVisible(true);
      jframe.setResizable(false);
      columns = new ArrayList<Rectangle>();

      bird = new Rectangle(WIDTH/2-10, HEIGHT/2-10, 20, 20);

      addColumn(true);
      addColumn(true);
      addColumn(true);
      addColumn(true);

      timer.start();
    }

    public void addColumn(boolean old)
    {
        int space = 300;
        int width = 100;
        int height = 50+rand.nextInt(300);

        if(old) {
            columns.add(new Rectangle(WIDTH + width + columns.size() * 300, HEIGHT - height-120, width, height));
            columns.add(new Rectangle(WIDTH + width + (columns.size() - 1) * 300, 0, width, HEIGHT - height - space));
        }
        else
        {
            columns.add(new Rectangle(columns.get(columns.size()-1).x+600, HEIGHT - height-120, width, height));
            columns.add(new Rectangle(columns.get(columns.size()-1).x, 0, width, HEIGHT - height - space));
        }
    }


    public void paintColumn(Graphics graphics, Rectangle column)
    {
       graphics.setColor(Color.gray.darker());
       graphics.fillRect(column.x,column.y,column.width,column.height);
    }

    public void repaint(Graphics graphics)
    {
      graphics.setColor(Color.gray);
      graphics.fillRect(0,0,WIDTH,HEIGHT);

      graphics.setColor(Color.RED);
      graphics.fillRect(bird.x,bird.y,bird.width,bird.height);

      graphics.setColor(Color.orange);
      graphics.fillRect(0,HEIGHT-120,WIDTH,120);

      graphics.setColor(Color.green);
      graphics.fillRect(0,HEIGHT-120,WIDTH,20);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        renderer.repaint();
    }

    public static void main(String[] args)
    {
        flappybird = new FlappyBird();
    }

}

